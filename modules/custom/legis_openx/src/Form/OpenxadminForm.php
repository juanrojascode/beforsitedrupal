<?php

namespace Drupal\legis_openx\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the search form for the muprespa previene section
 */
class OpenxadminForm extends FormBase {

  /**
   *
   * {@inheritdoc}
   *
   */
  public function getFormId() {
    return 'openx_form';
  }

  /**
   *
   * {@inheritdoc}
   *
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('legis_openx.settings');
    $server = $config->get('server');
    $serverId = $config->get('serverid');
    $form['server_openx'] = array(
      '#type' => 'textfield',
      '#title' => t('Server openx'),
      '#required' => TRUE,
      '#default_value' => isset($server) ? $server : '',
      '#description' => $this->t('Example: example.com/www/delivery'),
    );
    $form['server_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Server id'),
      '#required' => TRUE,
      '#default_value' => isset($serverId) ? $serverId : '',
    );
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    );
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::service('config.factory')->getEditable('legis_openx.settings');
    $server = $form_state->getValue('server_openx');
    $serverId = $form_state->getValue('server_id');
    $config->set('server', $server)->save();
    $config->set('serverid', $serverId)->save();
  }
}
