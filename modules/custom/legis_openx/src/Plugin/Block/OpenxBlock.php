<?php

namespace Drupal\legis_openx\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'OpenX' Block.
 *
 * @Block(
 *   id = "OpenX",
 *   admin_label = @Translation("Openx Block"),
 * )
 */
class OpenxBlock extends BlockBase {

	public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['data_zone_id'] = array(
      '#type' => 'number',
      '#title' => $this->t('Zone id'),
      '#description' => $this->t('Zone id'),
      '#default_value' => isset($config['data_zone_id']) ? $config['data_zone_id'] : '',
    );

    return $form;
  }
  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $data_zone_id = $form_state->getValue('data_zone_id');
    $this->configuration['data_zone_id'] = $data_zone_id;
  }
  /**
   * {@inheritdoc}
   */

  public function build() {
    $config = $this->getConfiguration();
    $var = \Drupal::config('legis_openx.settings');
    $server = $var->get('server');
    $serverid = $var->get('serverid');
    $numZone='0';
    if(is_numeric($config['data_zone_id']) && !empty($server) && !empty($serverid)){
      $numZone=$config['data_zone_id'];
      $this->return = '<ins data-revive-zoneid="'.$numZone.'" data-revive-id="'.$serverid.'"></ins>';
    }else{
      $this->return = '';
    }
    return array(
        '#type' => 'markup',
        '#markup' => $this->return,
        '#attributes' => array(
          'class' => array(
            "no-zone-".$numZone,
          ),
        ),
        '#attached' => array(
            'library' => array(
                'legis_openx/legis_openx.server',
            ),
            'drupalSettings' => array(
                'server' => $server,
                'serverid' => $serverid,
            ),
        ),
    );
  }
}